package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.saxutility.FlowerErrorHandler;
import com.epam.rd.java.basic.task8.controller.saxutility.FlowerHandler;
import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.*;

/**
 * Controller for SAX parser.
 */
public class SAXController {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private Set<Flower> flowers;
	private FlowerHandler handler = new FlowerHandler();
	private XMLReader reader;

	public SAXController() {
		// reader configuration
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = factory.newSAXParser();
			reader = saxParser.getXMLReader();
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace(); // log
		}
		reader.setErrorHandler(new FlowerErrorHandler());
		reader.setContentHandler(handler);
	}

	public Set<Flower> getFlowers() {
		return flowers;
	}

	public void buildSetFlowers(String xmlFileName) {
		try {
			reader.parse(xmlFileName);
		} catch (IOException | SAXException e) {
			e.printStackTrace(); // log
		}
		flowers = handler.getFlowers();
	}

	public List<Flower> getSortedFlowers() {
		List<Flower> flowersSorted = new ArrayList<>();
		flowersSorted.addAll(flowers);
		Collections.sort(flowersSorted, Comparator.comparing(Flower::getName).reversed());
		return flowersSorted;
	}
}

