package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowerXmlTag;
import org.xml.sax.helpers.DefaultHandler;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private Set<Flower> flowers;
	private XMLInputFactory inputFactory;
	public STAXController() {
		inputFactory = XMLInputFactory.newInstance();
		flowers = new HashSet<>();
	}
	public Set<Flower> getFlowers() {
		return flowers;
	}
	public void buildSetFlowers(String filename) {
		XMLStreamReader reader;
		String name;
		try(FileInputStream inputStream = new FileInputStream(filename)) {
			reader = inputFactory.createXMLStreamReader(inputStream);
			// StAX parsing
			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();
					if (name.equals(FlowerXmlTag.FLOWER.getValue())) {
						Flower flower = buildFlower(reader);
						flowers.add(flower);
					}
				}
			}
		} catch (XMLStreamException | IOException e) {
			e.printStackTrace();
		}
	}
	private Flower buildFlower(XMLStreamReader reader) throws XMLStreamException {
		Flower flower = new Flower();
		String name;

		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
				case XMLStreamConstants.START_ELEMENT : {
					name = reader.getLocalName();
					switch (FlowerXmlTag.valueOf(name.toUpperCase())) {
						case NAME : flower.setName(getXMLText(reader));
							break;
						case SOIL : flower.setSoil(getXMLText(reader));
							break;
						case ORIGIN : flower.setOrigin(getXMLText(reader));
							break;
						case MULTIPLYING : flower.setMultiplying(getXMLText(reader));
							break;
						case VISUALPARAMETERS : flower.setAppearance(getXMLVisualParameters(reader));
							break;
						case GROWINGTIPS : flower.setTips(getXMLTips(reader));
							break;
					}
				}
					break;
				case XMLStreamConstants.END_ELEMENT : {
					name = reader.getLocalName();
					if (FlowerXmlTag.valueOf(name.toUpperCase()) == FlowerXmlTag.FLOWER) {
						return flower;
					}
				}
					break;
			}
		}
		throw new XMLStreamException("Unknown element in tag <flower>");
	}

	private Flower.growingTips getXMLTips(XMLStreamReader reader) throws XMLStreamException {
		Flower.growingTips growingTips = new Flower().new growingTips();
		int type;
		String name;
		while (reader.hasNext()) {
			type = reader.next();
			switch (type) {
				case XMLStreamConstants.START_ELEMENT : {
					name = reader.getLocalName();
					switch (FlowerXmlTag.valueOf(name.toUpperCase())) {
						case TEMPRETURE : growingTips.setTemperature(getXMLAttribute(reader));
							break;
						case LIGHTING : growingTips.setLightRequiring(reader
								.getAttributeValue(null, "lightRequiring"));
							break;
						case WATERING : growingTips.setWatering(getXMLAttribute(reader));
							break;
					}
				}
					break;
				case XMLStreamConstants.END_ELEMENT : {
					name = reader.getLocalName();
					if (FlowerXmlTag.valueOf(name.toUpperCase()) == FlowerXmlTag.GROWINGTIPS) {
						return growingTips;
					}
				}
					break;
			}
		}
		throw new XMLStreamException("Unknown element in tag <growingTips>");
	}

	private Flower.visualParameters getXMLVisualParameters(XMLStreamReader reader) throws XMLStreamException {
		Flower.visualParameters visualParameters = new Flower().new visualParameters();
		int type;
		String name;
		while (reader.hasNext()) {
			type = reader.next();
			switch (type) {
				case XMLStreamConstants.START_ELEMENT : {
					name = reader.getLocalName();
					switch (FlowerXmlTag.valueOf(name.toUpperCase())) {
						case STEMCOLOUR : visualParameters.setStemColour(getXMLText(reader));
							break;
						case LEAFCOLOUR : visualParameters.setLeafColour(getXMLText(reader));
							break;
						case AVELENFLOWER : visualParameters.setAverageLength(getXMLAttribute(reader));
							break;
					}
					break;
				}
				case XMLStreamConstants.END_ELEMENT : {
					name = reader.getLocalName();
					if (FlowerXmlTag.valueOf(name.toUpperCase()) == FlowerXmlTag.VISUALPARAMETERS) {
						return visualParameters;
					}
					break;
				}
			}
		}
		throw new XMLStreamException("Unknown element in tag <visualParameters>");
	}

	private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
		String text = null;
		if (reader.hasNext()) {
			reader.next();
			text = reader.getText();
		}
		return text;
	}

	private String getXMLAttribute(XMLStreamReader reader) throws XMLStreamException {
		String text = null;
		String value;
		if (reader.hasNext()) {
			value = reader.getAttributeValue(0);
			reader.next();
			text = reader.getText() + " " + value;
		}
		return text;
	}

	public List<Flower> getSortedFlowers() {
		List<Flower> flowersSorted = new ArrayList<>();
		flowersSorted.addAll(flowers);
		Collections.sort(flowersSorted, Comparator.comparing(Flower::getName));
		return flowersSorted;
	}
}