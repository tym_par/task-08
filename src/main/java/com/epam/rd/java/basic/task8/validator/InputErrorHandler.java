package com.epam.rd.java.basic.task8.validator;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

public class InputErrorHandler implements ErrorHandler {
//    private static Logger logger = LogManager.getLogger();
    public void warning(SAXParseException e) {
//        logger.warn(getLineColumnNumber(e) + "-" + e.getMessage());
        System.out.println( "Warning in Input Handler at line : "+ e.getLineNumber());
    }
    public void error(SAXParseException e) {
//        logger.error(getLineColumnNumber(e) + " - " + e.getMessage());
        System.out.println( "Error in Input Handler at line : "+ e.getLineNumber());
    }
    public void fatalError(SAXParseException e) {
//        logger.fatal(getLineColumnNumber(e) + " - " + e.getMessage());
        System.out.println( "Fatal Error in Input Handler at line : "+ e.getLineNumber());
    }
    private String getLineColumnNumber(SAXParseException e) {
        // determine line and position of error
        return e.getLineNumber() + " : " + e.getColumnNumber();
    }
}
