package com.epam.rd.java.basic.task8.entity;

public enum FlowerXmlTag {

    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    MULTIPLYING("multiplying"),

    STEMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"),
    VISUALPARAMETERS("visualParameters"),

    TEMPRETURE("tempreture"),
    LIGHTING("lighting"),
    WATERING("watering"),
    GROWINGTIPS("growingTips");

    private String value;

    FlowerXmlTag(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
