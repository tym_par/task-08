package com.epam.rd.java.basic.task8.controller;

import java.io.IOException;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private Set<Flower> flowers;
	private DocumentBuilder docBuilder;
	public DOMController() {
		flowers = new HashSet<>();
		// configuration
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace(); // log
		}
	}
	public Set<Flower> getFlowers() {
		return flowers;
	}
	public void buildSetFlowers(String filename) {
		Document doc;
		try {
			doc = docBuilder.parse(filename);
			Element root = doc.getDocumentElement();
			// getting a list of <flower> child elements
			NodeList flowersList = root.getElementsByTagName("flower");
			for (int i = 0; i < flowersList.getLength(); i++) {
				Element flowerElement = (Element) flowersList.item(i);
				Flower flower = buildFlower(flowerElement);
				flowers.add(flower);
			}
		} catch (IOException | SAXException e) {
			e.printStackTrace(); // log
		}
	}
	private Flower buildFlower(Element flowerElement) {
		Flower flower = new Flower();
		if (flowerElement != null) {
			flower.setName(getElementTextContent(flowerElement, "name"));
			flower.setSoil(getElementTextContent(flowerElement, "soil"));
			flower.setOrigin(getElementTextContent(flowerElement, "origin"));
			flower.setMultiplying(getElementTextContent(flowerElement, "multiplying"));

			Flower.visualParameters visualParameters = flower.getAppearance();
			// init an visualParameters object
			Element appearanceElement =
					(Element) flowerElement.getElementsByTagName("visualParameters").item(0);
			visualParameters.setStemColour(getElementTextContent(appearanceElement, "stemColour"));
			visualParameters.setLeafColour(getElementTextContent(appearanceElement, "leafColour"));
			visualParameters.setAverageLength(getElementTextContent(appearanceElement, "aveLenFlower") + " " +
					appearanceElement
							.getElementsByTagName("aveLenFlower").item(0)
							.getAttributes().item(0)
							.getNodeValue());

			Flower.growingTips growingTips = flower.getTips();
			// init an growingTips object
			Element growingTipsElement =
					(Element) flowerElement.getElementsByTagName("growingTips").item(0);

			growingTips.setTemperature(getElementTextContent(growingTipsElement, "tempreture") + " " +
					growingTipsElement
							.getElementsByTagName("tempreture").item(0)
							.getAttributes().item(0)
							.getNodeValue());

			growingTips.setLightRequiring(growingTipsElement
					.getElementsByTagName("lighting").item(0)
					.getAttributes().item(0)
					.getNodeValue());

			growingTips.setWatering(getElementTextContent(growingTipsElement, "watering") + " " +
					growingTipsElement
					.getElementsByTagName("watering").item(0)
					.getAttributes().item(0)
					.getNodeValue());
		}
		return flower;
	}
	// get the text content of the tag
	private static String getElementTextContent(Element element, String elementName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		return node.getTextContent();
	}

	public List<Flower> getSortedFlowers() {
		List<Flower> flowersSorted = new ArrayList<>();
		flowersSorted.addAll(flowers);
		Collections.sort(flowersSorted,
				Comparator.comparing(Flower::getSoil).thenComparing(Flower::getOrigin));
		return flowersSorted;
	}
}
