package com.epam.rd.java.basic.task8.entity;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String multiplying;
    private visualParameters appearance = new visualParameters();
    private growingTips tips = new growingTips();
    public Flower() {
    }

    public Flower(String name, String soil, String origin, String multiplying, visualParameters appearance, growingTips tips) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.multiplying = multiplying;
        this.appearance = appearance;
        this.tips = tips;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public visualParameters getAppearance() {
        return appearance;
    }

    public void setAppearance(visualParameters appearance) {
        this.appearance = appearance;
    }

    public growingTips getTips() {
        return tips;
    }

    public void setTips(growingTips tips) {
        this.tips = tips;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nName: ");
        sb.append(name).append("\nSoil: ").append(soil);
        sb.append("\nOrigin: ").append(origin);
        sb.append("\nMultiplying: ").append(multiplying )
                .append(appearance).append(tips);
        return sb.toString();
    }

    public class visualParameters { // inner class
        private String stemColour;
        private String leafColour;
        private String averageLength;

        public visualParameters() {
        }

        public visualParameters(String stemColour, String leafColour, String averageLength) {
            this.stemColour = stemColour;
            this.leafColour = leafColour;
            this.averageLength = averageLength;
        }

        public String getStemColour() {
            return stemColour;
        }

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public String getAverageLength() {
            return averageLength;
        }

        public void setAverageLength(String averageLength) {
            this.averageLength = averageLength;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("\nvisualParameters:\n\tstemColour:");
            sb.append(stemColour).append("\n\tleafColour: ").append(leafColour);
            sb.append("\n\taverageLength: ").append(averageLength).append('\n');
            return sb.toString();
        }
    }

    public class growingTips{
        private String temperature;
        private String lightRequiring;
        private String watering;
        public growingTips() {
        }

        public growingTips(String temperature, String lightRequiring, String watering) {
            this.temperature = temperature;
            this.lightRequiring = lightRequiring;
            this.watering = watering;
        }

        public String getTemperature() {
            return temperature;
        }

        public void setTemperature(String temperature) {
            this.temperature = temperature;
        }

        public String getLightRequiring() {
            return lightRequiring;
        }

        public void setLightRequiring(String lightRequiring) {
            this.lightRequiring = lightRequiring;
        }

        public String getWatering() {
            return watering;
        }

        public void setWatering(String watering) {
            this.watering = watering;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("\ngrowingTips:\n\ttemperature: ");
            sb.append(temperature).append("\n\tlightRequiring: ").append(lightRequiring);
            sb.append("\n\twatering: ").append(watering).append('\n');
            return sb.toString();
        }
    }
}
