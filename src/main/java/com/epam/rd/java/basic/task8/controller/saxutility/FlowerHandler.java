package com.epam.rd.java.basic.task8.controller.saxutility;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowerXmlTag;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class FlowerHandler extends DefaultHandler {
    private final Set<Flower> flowers;
    private Flower current;
    private FlowerXmlTag currentXmlTag;
    private final EnumSet<FlowerXmlTag> withText;
    private static final String ELEMENT_FLOWER = "flower";
    private static Attributes atr;

    public FlowerHandler() {
        flowers = new HashSet<>();
        withText = EnumSet.range(FlowerXmlTag.NAME, FlowerXmlTag.WATERING);
    }
    public Set<Flower> getFlowers() {
        return flowers;
    }
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        if (ELEMENT_FLOWER.equals(qName)) {
            current = new Flower();
        } else {
            FlowerXmlTag temp = FlowerXmlTag.valueOf(qName.toUpperCase());
            if (withText.contains(temp)) {
                currentXmlTag = temp;
                atr = attrs;
            }
        }
    }
    @Override
    public void endElement(String uri, String localName, String qName) {
        if (ELEMENT_FLOWER.equals(qName)) {
            flowers.add(current);
        }
    }
    @Override
    public void characters(char[] ch, int start, int length) {
        String data = new String(ch, start, length).strip();
        if (currentXmlTag != null) {
            switch (currentXmlTag) {
                case NAME : current.setName(data);
                    break;
                case SOIL : current.setSoil(data);
                    break;
                case ORIGIN : current.setOrigin(data);
                    break;
                case MULTIPLYING : current.setMultiplying(data);
                    break;
                case STEMCOLOUR : current.getAppearance().setStemColour(data);
                    break;
                case LEAFCOLOUR : current.getAppearance().setLeafColour(data);
                    break;
                case AVELENFLOWER : current.getAppearance().setAverageLength(data + " " + atr.getValue(0));
                    break;
                case TEMPRETURE : current.getTips().setTemperature(data + " " + atr.getValue(0));
                    break;
                case LIGHTING : current.getTips().setLightRequiring(atr.getValue(0));
                    break;
                case WATERING : current.getTips().setWatering(data + " " + atr.getValue(0));
                    break;
                case VISUALPARAMETERS : current.getAppearance();
                    break;
                case GROWINGTIPS : current.getTips();
                    break;
                default : throw new EnumConstantNotPresentException(
                        currentXmlTag.getDeclaringClass(), currentXmlTag.name());
            }
        }
        currentXmlTag = null;
    }
}
