package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.xmlcreator.XmlDocumentCreator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {return;}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
//		// DOMController domController = new DOMController(xmlFileName);
		DOMController domController = new DOMController();
		domController.buildSetFlowers(xmlFileName);
//		System.out.println(domController.getFlowers());

		// sort (case 1)
		domController.getSortedFlowers();

		// save
		String outputXmlFile = "output.dom.xml";
		XmlDocumentCreator.xmlCreate(outputXmlFile, domController.getFlowers());

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController();
		saxController.buildSetFlowers(xmlFileName);
//		System.out.println(saxController.getFlowers());


		// sort  (case 2)
		saxController.getSortedFlowers();
		// save
		outputXmlFile = "output.sax.xml";
		XmlDocumentCreator.xmlCreate(outputXmlFile, saxController.getFlowers());

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
//		// STAXController staxController = new STAXController(xmlFileName);
		STAXController staxController = new STAXController();
		staxController.buildSetFlowers(xmlFileName);
//		System.out.println(staxController.getFlowers());

		// sort  (case 3)
		staxController.getSortedFlowers();

		// save
		outputXmlFile = "output.stax.xml";
		XmlDocumentCreator.xmlCreate(outputXmlFile, staxController.getFlowers());
	}

}
