package com.epam.rd.java.basic.task8.xmlcreator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlDocumentCreator {
    public static void xmlCreate(String fileName, Collection<Flower> flowers) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        // forming a document tree
        Document document = documentBuilder.newDocument();
        String root = "flowers";
        Element rootElement = document.createElement(root);
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        document.appendChild(rootElement);
        // adding elements information
        Element element;
        String value;
        for (Flower f : flowers) {
            Element flower = document.createElement("flower");
            element = document.createElement("name");
            element.setTextContent(f.getName());
            flower.appendChild(element);
            element = document.createElement("soil");
            element.setTextContent(f.getSoil());
            flower.appendChild(element);
            element = document.createElement("origin");
            element.setTextContent(f.getOrigin());
            flower.appendChild(element);

            // adding visualParameters
            Element visualParameters = document.createElement("visualParameters");

            element = document.createElement("stemColour");
            element.setTextContent(f.getAppearance().getStemColour());
            visualParameters.appendChild(element);

            element = document.createElement("leafColour");
            element.setTextContent(f.getAppearance().getLeafColour());
            visualParameters.appendChild(element);

            element = document.createElement("aveLenFlower");
            value = f.getAppearance().getAverageLength();
            element.setTextContent(value.substring(0, value.length() - 3));
            element.setAttribute("measure", "cm");
            visualParameters.appendChild(element);

            flower.appendChild(visualParameters);

            // adding growingTips
            Element growingTips = document.createElement("growingTips");
            element = document.createElement("tempreture");
            value = f.getTips().getTemperature();
            element.setTextContent(value.substring(0, value.length() - 8));
            element.setAttribute("measure", "celcius");
            growingTips.appendChild(element);

            element = document.createElement("lighting");
            value = f.getTips().getLightRequiring();
            element.setAttribute("lightRequiring", value);
            growingTips.appendChild(element);

            element = document.createElement("watering");
            value = f.getTips().getWatering();
            element.setTextContent(value.substring(0, value.length() - 10));
            element.setAttribute("measure", "mlPerWeek");
            growingTips.appendChild(element);

            flower.appendChild(growingTips);

            element = document.createElement("multiplying");
            element.setTextContent(f.getMultiplying());
            flower.appendChild(element);
            rootElement.appendChild(flower);

        }
        // write tree to file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new FileWriter(fileName));
            transformer.transform(source, result);
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }
    }
}
